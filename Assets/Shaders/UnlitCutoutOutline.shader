﻿Shader "Game/UnlitCutoutOutline"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_OutlineSize("Outline size:", float ) = 0.100
		_OutlineColor("Outline color:", color) = (0.0, 0.0, 0.0, 1.0)
		_CutoutThreshold("Cutout threshold:", float) = 0.5
	}
	SubShader
	{
		Tags { "Queue"="AlphaTest" }
		LOD 100

//		Zwrite Off
		Alphatest LEqual [_CutoutThreshold]

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _OutlineSize;
			float4 _OutlineColor;
			float _CutoutThreshold;

			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex * _OutlineSize);
				o.uv = v.uv; //TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{				
				fixed4 col = tex2D(_MainTex, i.uv);
				clip(col.w - _CutoutThreshold);
				return _OutlineColor;
			}
			ENDCG
		}

		Alphatest Less [_CutoutThreshold]
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _OutlineColor;
			float _CutoutThreshold;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				clip(col.w - _CutoutThreshold);
				// apply fog
				return col;
			}
			ENDCG
		}

	}
}
