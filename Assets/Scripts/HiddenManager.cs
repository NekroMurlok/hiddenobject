﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HiddenManager : MonoBehaviour {

    public List<Texture2D> m_textureSet = new List<Texture2D>();
    public HiddenObject m_hiddenObjectPrefab = null;
    public float m_objectSize = 0.1f;
    public float m_objectSizeRamdomness = 0.1f;

    public int m_numObjects = 100;

    public string m_hiddenObjectLayer;

    public Texture2D m_objectToFind = null;

    private Rect m_viewport;
    private List<HiddenObject> m_objects = new List<HiddenObject>();

    private Camera cam = null;

    private int m_find;
    private int m_objectToFindCount;
    private int m_layerMask;

    public Text m_ScoreText = null;
    public Text m_LeftText = null;
    public RawImage m_objectToFindImage = null;

    // Use this for initialization
    void Awake()
    {
        for (int c = 0; c < m_numObjects; c++)
        {
            HiddenObject obj = Instantiate<HiddenObject>(m_hiddenObjectPrefab);
            obj.transform.rotation = Quaternion.Euler(90.0f, 180.0f, 0.0f);
            obj.transform.gameObject.name = "HiddenObject" + c.ToString();
            m_objects.Add(obj);
        }

        m_layerMask = LayerMask.GetMask(m_hiddenObjectLayer);

    }

    void Start () {
        cam = Camera.main;
        Vector3 extentsMax = cam.ScreenToWorldPoint(new Vector3(cam.pixelWidth, cam.pixelHeight, 100.0f));
        Vector3 extentsMin = cam.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, 100.0f));
        m_viewport = new Rect(extentsMin.x, extentsMin.y, extentsMax.x - extentsMin.x, extentsMax.y - extentsMin.y);
        InitMix();

        m_objectToFindImage.texture = m_objectToFind;
        setScore(0);


        m_find = 0;
	}

    void InitMix()
    {
        m_find = m_objectToFindCount = 0;
        for (int c = 0; c < m_numObjects; c++)
        {
            float objSize = m_objectSize + Random.value * m_objectSizeRamdomness;
            Vector3 position = new Vector3(m_viewport.x + objSize + Random.value * (m_viewport.width - objSize * 2.0f),
                                           m_viewport.y + objSize + Random.value * (m_viewport.height - objSize * 2.0f),
                                           100.0f);
            HiddenObject obj = m_objects[c];
            obj.transform.position = position;
            obj.transform.rotation = Quaternion.Euler(0.0f, 0.0f, Random.value * 360.0f);
//            obj.transform.Rotate(Vector3.up, Random.value * 360.0f);
            obj.transform.localScale = Vector3.one * objSize;
            int idx = (int)(Random.value * m_textureSet.Count);
            Texture2D currentObject = m_textureSet[idx];
            obj.TileTexture = currentObject;
            if (currentObject == m_objectToFind)
            {
                m_objectToFindCount++;
            }
            obj.gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update () {
		if (Input.GetKeyDown(KeyCode.Space))
        {
            InitMix();
            setScore(0);
        }

#if UNITY_ANDROID || UNITY_IOS
        if (Input.touches.Length > 0)
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
                {
                    dispatchTouch(touch.position);
                }
            }

        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                dispatchTouch(Input.mousePosition);
            }
            else if (Input.GetMouseButton(0))
            {
                dispatchTouch(Input.mousePosition);
            }
        }

#elif UNITY_EDITOR || UNITY_STANDALONE_WIN
			if (Input.GetMouseButtonDown(0)) {
				dispatchTouch(Input.mousePosition);
			} else if (Input.GetMouseButton(0)) {
				dispatchTouch(Input.mousePosition);
			}
#endif

    }

    void dispatchTouch(Vector3 pos)
    {
        //Vector3 position = pos;
        //Ray ray = cam.ScreenPointToRay(pos);
        //        RaycastHit hit;
        //        if (Physics.Raycast(ray, out hit, 2000.0f, m_layerMask))

        //        Vector2 position = new Vector2(ray.origin.x, ray.origin.y);
        pos.z = 100.0f;
        Vector3 position = cam.ScreenToWorldPoint(pos);
        RaycastHit2D hit;
        hit = Physics2D.Raycast(position, Vector2.up, Mathf.Infinity, m_layerMask, -10.0f);

        if (hit.transform != null)
        {
            //Debug
//            Renderer rend = hit.transform.GetComponent<Renderer>();
//            rend.material.SetColor("_OutlineColor", Color.red);
//            Debug.Log("worldposition: " + position.ToString() + "Gameobject name: " + hit.transform.gameObject.name);

            HiddenObject hObj = hit.transform.gameObject.GetComponent<HiddenObject>();
            if (hObj != null)
            {
                if (hObj.TileTexture == m_objectToFind)
                {
                    hObj.gameObject.SetActive(false);
                    setScore(++m_find);
                }
            }
        }

    }

    void setScore(int score)
    {
        m_ScoreText.text = "SCORE: " + score.ToString();
        m_LeftText.text = "LEFT:\n" + (m_objectToFindCount - score).ToString();
    }
}
