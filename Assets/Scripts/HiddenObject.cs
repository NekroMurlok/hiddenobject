﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HiddenObject : MonoBehaviour {

    public Texture2D m_texture;
    public float zRadius = 2.0f;

//    private float initAngle;

    private Material m_material;
	// Use this for initialization
	public void Awake () {
//        m_material = new Material(Shader.Find("Unlit/Transparent Cutout"));
        MeshRenderer rend = GetComponent<MeshRenderer>();
        m_material = rend.material;
        TileTexture = m_texture;

//        initAngle = Random.value * 360.0f;
    }


    public Texture2D TileTexture
    {
        get { return m_texture; }
        set {
            m_texture = value;
            m_material.SetTexture("_MainTex", m_texture);
        }
    }
/*
    // Update is called once per frame
    void Update () {
        Vector3 position = transform.position;
        position.z = 100.0f + Mathf.Sin((initAngle + Time.time) * Mathf.Deg2Rad) * zRadius;
        transform.position = position;
	}
    */
}
